Curso de Udemy "Crea una LANDING PAGE moderna con HTML CSS Y Javascript" por Jordan Alexander
Duración: 1 hora

Recursos usados para crear el proyecto:

📌 Freepick:
Un banco de ilustraciones el cual nos ayudará a complementar la landing page, no olvides atribuir al autor si usas las imagenes sin la licencia freemium.
https://www.freepik.com/

📌 Ui-gradients:
Un generador de gradientes, el cuál le dará ese estilo tan único y novedoso a nuestra pagina.
https://uigradients.com/

📌 Google fonts:
Google, te proporciona una herramienta donde puedes integrar muchas de tipografías en tu web.
https://fonts.google.com/

📌 Generador SVG WAVE:
https://smooth.ie/blogs/news/svg-wavey-transitions-between-sections

📌 Clip-path maker:
Un generador de Clip-path, el cuál nos ayudará a hacer el fabuloso efecto del menú animado.
https://bennettfeely.com/clippy/